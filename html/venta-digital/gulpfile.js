var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var connect = require('gulp-connect');


gulp.task('connect', function(){
  connect.server({
      root:'./dist',
      port:4232
  });

});


var csslibs=[
  './src/libs/css/owl.carousel.css'
]
//  Css libs
gulp.task('csslibs', function () {
  gulp.src('./src/sass/libs.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/assets/css/'));
});

//  Js libs
var jslibs = [
  './src/js/vendor/jquery-1.12.0.min.js',
  './src/js/vendor/jquery-ui.min.js',
  './src/js/vendor/jquery.alphanumeric.pack.js',
  './src/js/vendor/jquery.validate.js'
]
gulp.task('jslibs', function () {
  gulp.src(jslibs)
  .pipe(concat('libs.js'))
  .pipe(gulp.dest('./src/js/'));
});


//  Js Concat
var jsFilesToConcat = [
  './src/js/libs.js',
  './src/js/main.js'
]
gulp.task('concatJs', function () {
  gulp.src(jsFilesToConcat)
  .pipe(concat('init.js'))
  .pipe(gulp.dest('./dist/assets/js/'));
});


// Css Concat
var cssFilesToConcat = [
  './dist/assets/css/libs.css',
  './dist/assets/css/main.css'
]
gulp.task('concatCss', function () {
  gulp.src(cssFilesToConcat)
  .pipe(concat('style.css'))
  .pipe(gulp.dest('./dist/assets/css/'));
});



// Sass
gulp.task('sass', function () {
  gulp.src('./src/sass/main.scss')
  	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/assets/css/'));
});


// Jade
gulp.task('jade', function() {
  gulp.src(['./src/jade/**/*.jade','!./src/jade/**/_*.jade'])
    .pipe(jade({
    	pretty: true
    }))
    .on('error', function () { console.log(arguments); })
    .pipe(gulp.dest('./dist/'))
});


gulp.task('sprite', function () {
	var spriteData = gulp.src('./src/images/*.*')
  					.pipe(spritesmith({
    					imgName: 'sprite.png',
   						cssName: '_sprite.scss',
              imgPath:'../images/sprite.png',
              padding:10
	}));

    spriteData.img.pipe(gulp.dest('./dist/assets/images'))
    spriteData.css.pipe(gulp.dest('./src/sass/atoms/'));
});




// // Watch
gulp.task('watch', function () {
    // gulp.watch('./src/sass/**/*.scss',['sass']);
    // gulp.watch('./src/jade/**/*.jade',['jade']);

    watch('./src/sass/**/*.scss', function(event) {
      gulp.start('sass');
    });
    watch('./src/jade/**/*.jade', function(event) {
      gulp.start('jade');
    });

    watch(['./dist/assets/css/libs.css','./dist/assets/css/main.css'], function(event) {
      gulp.start('concatCss');
    });

   watch(['./src/js/libs.js','./src/js/main.js'], function(event) {
      gulp.start('concatJs');
    });


});

gulp.task('default', ['sass', 'jade','watch','connect']);
 







