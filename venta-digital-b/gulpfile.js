var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var connect = require('gulp-connect');

gulp.task('connect', function(){
  connect.server({
      root:'./dist',
      port:3457
  });

});


// Sass
gulp.task('sass', function () {
  gulp.src('./src/sass/style.scss')
  	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/assets/css/'));
});


// Jade
gulp.task('jade', function() {
  gulp.src(['./src/jade/**/*.jade','!./src/jade/**/_*.jade'])
    .pipe(jade({
    	pretty: true
    }))
    .on('error', function () { console.log(arguments); })
    .pipe(gulp.dest('./dist/'))
});






// // Watch
gulp.task('watch', function () {

    watch('./src/sass/**/*.scss', function(event) {
      gulp.start('sass');
    });
    watch('./src/jade/**/*.jade', function(event) {
      gulp.start('jade');
    });

});

gulp.task('default', ['sass', 'jade','connect','watch']);








