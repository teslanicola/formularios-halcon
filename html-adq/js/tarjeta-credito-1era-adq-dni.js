jQuery.noConflict();
jQuery(document).ready(function($) {

    $('#lq-btn-verify').click(function(e) {
        e.preventDefault();
        var btn = $(this);
        var form = $('#frData');


        if(!btn.hasClass('disabled')){
            btn.addClass('disabled');            		
			valid = form.simpleValidate();

			if(valid){
                // send form ajax 				
				// console.log('send form');
				btn.removeClass('disabled');
                
                form.submit();
                $('.lq-formbig').addClass('form-submited');
                $('.lq-points-form').hide();
                setTimeout(function(){
                    $('.lq-points-form').remove();
                },3000);


            } else {
                //$('.lq-div-alert').text("Completa correctamente los campos marcados.");
                btn.removeClass('disabled');

            }
        }
    });
	
	$('#lq-data2, #lq-data3, #lq-data4').click(function(e) {
        var valid  = $(this).prev('div').simpleValidate({'painterror':false});
		var parent = $(this).find('.lq-parent');
		
		if(valid){
			$('.lq-main .lq-points .lq-bar').css('height', parent.position().top + 20);
			parent.find('.lq-point-default').addClass('lq-ok');
		}else{
			$('.lq-main .lq-points .lq-bar').css('height', 0);
			parent.find('.lq-point-default').removeClass('lq-ok');
		}
    });

    $('#itipodoc_cony').change(function(e) {
        var idoc = $('#inumdoc_cony');
        if($(this).val() == '1'){
            idoc.attr('data-valid','dni').attr('maxlength','8');
            idoc.off('.alphanum').numeric('positiveInteger');
            $('#dataconyugue_ce').fadeOut().find('input').addClass('novalid');
            $('#iape_materno_cony').attr('data-valid','required');
        }
        if($(this).val() == '2'){
            idoc.attr('data-valid','ce').attr('maxlength','11');
            idoc.off('.alphanum').alphanum();
            $('#dataconyugue_ce').fadeIn().find('input').removeClass('novalid');
            $('#iape_materno_cony').removeAttr('data-valid').parent().parent().removeClass('error');
        }
    });     
	
	$('#iestado').change(function(e) {
        if($(this).val() == '5' || $(this).val() == '7'){
			$('#lq-data5').fadeIn().find('input:visible, select:visible').removeClass('novalid');;
		}else{
			$('#lq-data5').fadeOut().find('input:visible, select:visible').addClass('novalid');;
		}

        var t = $("#iestado option:selected").text();
        $('#iestado_hidden').val(t);

    });
	
	$('.change_situacion select').change(function(e) {
		var parent = $(this).parent().parent().parent();
		var fixruc = parent.find('.fixruc');
		
        if($(this).val() == '10' || $(this).val() == '11' || $(this).val() == ''){
			fixruc.fadeOut();
			fixruc.find('input').removeAttr('data-valid');
		}else{
			fixruc.fadeIn();
			fixruc.find('input').attr('data-valid','ruc');
		}
    });



    // Arreglado camptura de datos

    
    $('#isituacion').change(function(e) {
        var t = $("#isituacion option:selected").text();
        $('#isituacion_hidden').val(t);
    });
    $('#iestado').change(function(e) {
        var t = $("#iestado option:selected").text();
        $('#iestado_hidden').val(t);
    });


    $('#iestado_cony').change(function(e) {
        var t = $("#iestado_cony option:selected").text();
        $('#iestado_cony_hidden').val(t);
    });
    $('#isituacion_cony').change(function(e) {
        var t = $("#isituacion_cony option:selected").text();
        $('#isituacion_cony_hidden').val(t);
    });


    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };


    var documento = getUrlParameter('documento');
    var telefono = getUrlParameter('telefono');
    var correo = decodeURIComponent(getUrlParameter('correo'));

    $('#iemail').val(correo);
    $('#nro_documento').val(documento);
    $('#icelular').val(telefono);

    console.log(documento, correo, telefono);






	
});









