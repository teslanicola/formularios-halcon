jQuery.noConflict();
jQuery(document).ready(function($) {
	$.fn.simpleValidate = function(options) {
		var defaults = { 'errorclass':'error','errorempty':'lq-empty','painterror':true };
		var opts = $.extend(defaults, options);
		var bool = true, finish = true, elem;
		var parent = [];
		var msgempty = '';
		var inputs = this.find('*[data-valid]:not(.novalid)');
		
		inputs.each(function(){
			elem = $(this);
			parent = elem.parent().parent();
			msgempty = parent.find('.lq-msgempty');
			
			switch(elem.attr('data-valid')){
				case 'email': 		bool = validEmail(elem.val()); break;
				case 'required':	bool = validRequired(elem);  break;
				case 'integer': 	bool = validInt(elem.val());  break;
				case 'dni': 		bool = validDni(elem.val()); break;
				case 'ce': 			bool = validCE(elem.val()); break;
				case 'celu': 		bool = validCelu(elem.val()); break;
				case 'ruc': 		bool = validRuc(elem.val()); break;
				case 'pasaporte': 	bool = validPasaporte(elem.val()); break;
			}				
			if(!bool || elem.val() == elem.attr('data-holder')){
				if(opts.painterror){ parent.addClass(opts.errorclass);}
				if(($.trim(elem.val()) == '' || elem.val() == elem.attr('data-holder')) && msgempty.length > 0){
					parent.addClass(opts.errorempty);
				}else{
					parent.removeClass(opts.errorempty);
				}
				finish = false;
				bool = true;				
			}else{
				parent.removeClass(opts.errorclass);
				parent.removeClass(opts.errorempty);
			}			
			if(parent.hasClass('lq-ingroup')){
				if(parent.parent().hasClass('lq-group-element')){
					parent.parent().find('.lq-parent .lq-msgvalid').css('display','none');
					parent.parent().find('.lq-parent.error .lq-msgvalid:eq(0)').css('display','block');
				}
			}			
		});	
		
		function validEmail(valor){ return (/^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(valor)) ? true : false; }
		function validInt(valor){ return (/^\d+$/.test(valor)) ? true : false; }
		function validDni(valor){ return (/^\d+$/.test(valor) && valor.length == 8) ? true : false; }
		function validRuc(valor){ return (/^\d+$/.test(valor) && valor.length <= 11) ? true : false; }	
		function validCelu(valor){ return (/^\d+$/.test(valor) && valor.length >= 6 && valor.length <= 9) ? true : false; }
		function validCE(valor){ return (valor.length >= 4 && valor.length <= 11) ? true : false; }
		function validPasaporte(valor){ return (valor.length >= 7 && valor.length <= 11) ? true : false; }
		function validRequired(elem){ 
			if(elem.is('select')){ return ($.trim(elem.val()) != '') ? true : false;
			}else if(elem.is('input[type=checkbox]')){ return (elem.prop('checked')) ? true : false;
			}else{ return (/[^.*\s]/.test(elem.val())) ? true : false; }
		}
			
		return finish;
	};
	$.fn.showError = function(map){
		var obj = $.map(map,function(value,key){
			return { obj: key, msg: value };
		});
		for (var i in obj) {
			var parent = this.find('*[name='+obj[i].obj+']').parent().parent();
			parent.addClass('error');
			if(obj[i].msg != ''){ parent.find('.lq-msgvalid').text(obj[i].msg); }
		}
	};
	$.fn.normalize = function() {
		var inputs = this.find('input,textarea');		
		inputs.each(function(){
			elem = $(this);
			if (elem.val() == elem.attr('data-holder')){
				elem.val('');
			}
		});	
	};    

	$.fn.simpleBox = function(options) {
		var defaults = {'closed' : $('.lq-boxclose')};
		var opt = $.extend(defaults, options);
		var overlay = $('#lq-overlay');
		
		if(overlay.length <= 0){						
			overlay = $('<div id="lq-overlay"></div>').appendTo($('body'));
		}
		
		this.each(function(){			
			$(this).bind('click',function(e){	
				e.preventDefault();
				var box = $('#'+$(this).attr('box'));
				var boxbody = box.find('.ld-termsdesc');	
				var h = $(window).height();
				
				box.css({'height':h*0.9, 'top':h*0.05});
				boxbody.css('height',h*0.9 - 180);
				overlay.css('height',$(document).height());
				overlay.fadeIn(300,function(){
					box.fadeIn(500);
				});		
			});	
		});
		
		opt.closed.click(function(e){
			e.preventDefault();
			$(this).parent().parent().fadeOut();	
			overlay.fadeOut(300);		
		});	

		overlay.click(function(e){ opt.closed.click(); });
	};
	
	$.fn.setLoading = function(opt){
		var loader = $('#lq-overloader');
		if(opt === 'show'){ loader.fadeIn(); }
		if(opt === 'hide'){ loader.fadeOut(); }
	}
	
	$('*[box]').simpleBox();    
    $(".lq-number input").numeric('positiveInteger');
    $(".lq-alpha input").alpha();    
    $(".lq-alphanum input").alphanum();
	$('.lq-input, .lq-pull').prepend('<em></em>');
	$('<div id="lq-overloader"><img src="img/loading.gif"></div>').appendTo($('.lq-main'));	
	$('.lq-summary ul').each(function(){
		var li = $(this).find('li');
		li.first().css('min-height',li.last().height())
	});
    
    // input	
	$(".lq-input input").each(function(i, e) {
		var $input = $(this);		
		if ($input.val() == "" && $input.attr("data-holder") != "") {  $input.val($input.attr("data-holder")); }
		$input.focus(function() { 
			$input.parent().addClass("focus"); 
			if ($input.val() == $input.attr("data-holder")) { $input.val(""); }	
		});
		$input.blur(function() { 
			$input.parent().removeClass("focus"); 
			if ($input.val() == "") { 
				$input.val($input.attr("data-holder"));
				$input.parent().removeClass("ok");
			}else{				
				$input.parent().addClass("ok");
			}
			$input.parent().simpleValidate();
		});			
	});
	$('.lq-content-inputs .lq-input input').focus(function() { 	
		var parent = $(this).parents('.lq-parent');
		$('.lq-points .lq-point-focus').show().css('top',parent.position().top + parseInt(parent.css('marginTop')) + 18);
	});
	
	$('.lq-once input').blur(function(e) {
		var  val = parseInt($(this).val()) || 0;
        if(val > 11){$(this).val('').parent().parent().addClass('error'); 
		}else{$(this).val(val).parent().parent().removeClass('error');}
    });
    // pull
    $(".lq-pull select").each(function(i, e) {
		var $pull = $(this);
		$pull.parent().prepend("<div>"+$pull.children(":selected").text()+"</div>");
		if($pull.children("option[value='']").length == 0){$pull.parent().addClass('ok');}		
		
		$pull.change(function(){
			$pull.parent().children("div").text($pull.children(":selected").text());
			if ($pull.val() == "") { 
				$pull.parent().removeClass('ok');
			}else{
				$pull.parent().addClass("ok");
			}
			$pull.parent().simpleValidate();
		});
		
		$pull.click(function(e) {
            $('.lq-points .lq-point-focus').show().css('top',$pull.parents('.lq-parent').position().top + 18);
        });
    });
	
    // checkbox
    $(".lq-check").each(function(i, e) {
        var $this = $(this);
        var $check = $this.find('input');
        
        $this.click(function(){
            if ($this.hasClass("checked")) {
                $check.prop('checked', false);
                $this.removeClass("checked");
            } else {
                $check.prop('checked', true);
                $this.addClass("checked");
            }
        });
        $check.change(function(){
           if ($check.is(':checked')) {
               $this.addClass("checked");
           } else {
               $this.removeClass("checked");
           }
        });
    });
	//tooltip
	$('.lq-tooltip').mouseenter(function(){
		$(this).find('.lq-tooltip-body').stop(true,true).fadeIn();
	}).mouseleave(function(e) {
        $(this).find('.lq-tooltip-body').stop(true,true).fadeOut();
    });

});