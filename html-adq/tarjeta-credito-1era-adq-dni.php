<!doctype html>
<html class="no-js aui" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Formularios Halcon</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"> 
</head>
<body>


<!-- ADQ form -->
<link rel="stylesheet" href="http://hc-visaoromembresia-html.apps-interbank.com/html-adq/css/main.css">




<div class="lq-main">
    
    <!-- #Header -->
	<div class="lq-header">
    	<div class="lq-logo">
        	<div class="lq-wrap"><img src="img/logo.png" alt="Interbank"></div>
        </div>
        <div class="lq-breadcrumb">
        	<div class="lq-wrap">
            	<i></i><a href="">Tarjetas de Crédito</a>
            </div>
        </div>
    </div>
    <!-- Header -->

    <div class="lq-content">
    	<div class="lq-formbig">
        	<h2 class="lq-login-h2"><i></i>
                <strong>¡Hola! </strong><br>
                Para pre-calificarte para <span>Tarjeta de Crédito</span>, por favor completa estos datos adicionales
            </h2>
            <div class="lq-points-form">
            	<div class="lq-points"><div>
                	<div class="lq-bar"></div>
                    <div class="lq-point-focus"></div>
                    <div class="lq-sup-bar"></div>
                </div></div>
                <script>
                    var submitted=false;

                </script>
                <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(submitted){}">
                    
                </iframe>

                <form method="POST" action="https://docs.google.com/forms/d/e/1FAIpQLSeWD4oUueGoJQNX8jB-Oag2FuUnVjq8NtdlSPC_Z2ap0EthaQ/formResponse" target="hidden_iframe" onsubmit="submitted=true;" id="frData" class="lq-point-form"  autocomplete="off">


                    <div class="lq-content-inputs">
                        <div id="lq-data1">         
                            <input type="hidden" name="entry.1722276055" id="nro_documento"/>

                            <div class="lq-parent">
                                <div class="lq-input lq-alpha">
                                    <input type="text" name="entry.1627212854" id="iape_paterno" data-valid="required" data-holder="Apellido paterno" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                                <div class="lq-point-default lq-ok"></div>
                                <div class="lq-point-default-txt">Datos personales *</div>
                            </div>
                            <div class="lq-parent">
                                <div class="lq-input lq-alpha">
                                    <input type="text" name="entry.516228196" id="iape_materno" data-valid="required" data-holder="Apellido materno" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                            </div>
                            <div class="lq-parent">
                                <div class="lq-input lq-alpha">
                                    <input type="text" name="entry.146617301" id="inombres" data-valid="required" data-holder="Primer nombre" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                            </div>
                            <div class="lq-parent">
                                <div class="lq-input lq-alpha">
                                    <input type="text" name="entry.1378678761" id="iotros_nombres" data-holder="Otros nombres" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                            </div>
                            
                            <div class="lq-parent">
                                <div class="lq-input">
                                    <input type="text" name="entry.798299571" id="iemail" data-valid="email" data-holder="Correo" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                            </div>
                            <div class="lq-parent">
                                <div class="lq-input lq-number">
                                    <input type="text" name="entry.226017163" id="icelular" data-valid="celu" data-holder="Celular" maxlength="9" />
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                            </div>
                            
                            <h6 class="lq-h6">Fecha de nacimiento</h6>
                            <div class="lq-group-element">
                                <div class="lq-parent lq-ingroup lq-dia">
                                    <div class="lq-pull">
                                        <select name="entry.1532580939" id="idia" data-valid="required">
                                            <option value="">Día</option>
                                            <?php
                                                for($i = 1; $i<=31; $i++){
                                            ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <span class="lq-msgvalid">Ingresar el día</span>
                                </div>
                                <div class="lq-parent lq-ingroup lq-mes ">
                                    <div class="lq-pull lq-noborder">
                                        <select name="entry.1409282885" id="imes" data-valid="required">
                                            <option value="">Mes</option>
                                            <option value="1">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Setiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                    </div>
                                    <span class="lq-msgvalid">Ingresar el mes</span>
                                </div>
                                <div class="lq-parent lq-ingroup lq-anio ">
                                    <div class="lq-pull lq-noborder">
                                        <select name="entry.1467138348" id="ianio" data-valid="required">
                                            <option value="">Año</option>
                                            <?php
                                                for($i = 1910; $i<=1998; $i++){
                                            ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <span class="lq-msgvalid">Ingresar el año</span>
                                </div>
                                <div class="both"></div> 
                            </div>
                        </div>
                              
                        <div id="lq-data2" style="padding-top:60px">   
                            <div class="change_situacion">
                                <div class="lq-parent">
                                    <div class="lq-pull">
                                        <select name="isituacion" id="isituacion" data-valid="required">
                                            <option value="">Seleccione situación laboral</option>
                                            <option value="1">Dependiente</option>
                                            <option value="3">Accionista empresa</option>
                                            <option value="4">Profesional independiente</option>
                                            <option value="5">Rentista 1 categoría</option>
                                            <option value="6">Rentista 2 categoría</option>
                                            <option value="7">Microempresario formal</option>
                                            <option value="9">Jubilado</option>
                                            <option value="10">Informal</option>
                                            <!--<option value="11">No laboro actualmente</option>-->
                                        </select>
                                        <input type="hidden" name="entry.1719173885" id="isituacion_hidden" value="">
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                    <div class="lq-point-default"></div>
                                	<div class="lq-point-default-txt">Datos laborales *</div>
                                </div>
                                <div class="lq-parent fixruc">
                                    <div class="lq-input lq-number">
                                        <input type="text" name="entry.122317144" id="iruc" data-holder="Nro RUC" maxlength="11">
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                    <div class="lq-input-info">Encuenta tu RUC <a href="#">aquí</a></div>
                                    
                                </div>
                            </div>                   
                        </div>
                        
                        	
                            
                        <div id="lq-data4" style="padding-top:60px">
                            <div class="lq-parent">
                                <div class="lq-pull">
                                    <select name="iestado" id="iestado" data-valid="required">
                                        <option value="">Seleccione estado civil</option>
                                        <option value="1">Soltero</option>
                                        <option value="4">Divorciado</option>
                                        <option value="5">Conviviente</option>
                                        <option value="6">Viudo</option>
                                        <option value="7">Casado</option>
                                        <option value="8">Casado con Bienes separados</option>
                                    </select>
                                    <input type="hidden" name="entry.1066132455" id="iestado_hidden" value="">
                                </div>
                                <span class="lq-msgvalid">Ingresar correctamente</span>
                                <div class="lq-point-default"></div>
                                <div class="lq-point-default-txt">Estado civil *</div>
                            </div> 
                               
                            <div id="lq-data5" style="display:none">
                                <h6 class="lq-h6">Datos del cónyuge *</h6>
                                <div class="lq-parent">
                                    <div class="lq-w50p">
                                        <div class="lq-pull">
                                            <select name="entry.1063274690" id="itipodoc_cony" data-valid="required"  class="novalid">
                                                <option value="1">DNI</option>
                                                <option value="2">Carnet de extranjería</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="lq-w50p ml1">
                                        <div class="lq-input lq-number lq-noborder">
                                            <input type="text" name="entry.25347935" id="inumdoc_cony" data-valid="dni" maxlength="8" data-holder="Número"  class="novalid">
                                        </div>
                                        <span class="lq-msgvalid">Ingresar correctamente</span>
                                    </div>
                                    
                                    <div class="both"></div>
                                </div>
                                <div class="lq-parent">
                                    <div class="lq-input lq-alpha">
                                        <input type="text" name="entry.1998187709" id="iape_paterno_cony" data-valid="required" data-holder="Apellido paterno" class="novalid" />
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                </div>
                                <div class="lq-parent">
                                    <div class="lq-input lq-alpha">
                                        <input type="text" name="entry.1481131561" id="iape_materno_cony" data-valid="required" data-holder="Apellido materno" class="novalid" />
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                </div>
                                <div class="lq-parent">
                                    <div class="lq-input lq-alpha">
                                        <input type="text" name="entry.1762114545" id="inombres_cony" data-valid="required" data-holder="Primer nombre" class="novalid" />
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                </div>
                                <div class="lq-parent">
                                    <div class="lq-input lq-alpha">
                                        <input type="text" name="entry.271642758" id="iotros_nombres_cony" data-holder="Otros nombres" />
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                </div>
                                <div class="lq-group-element">
                                	<h6 class="lq-h6">Fecha de nacimiento del cónyugue *</h6>
                                    <div class="lq-parent lq-ingroup lq-dia">
                                        <div class="lq-pull">
                                            <select name="entry.237910961" id="idia_cony" data-valid="required" class="novalid">
                                                <option value="">Día</option>
                                                <?php
                                                for($i = 1; $i<=31; $i++){
                                                ?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php
                                                    }
                                                ?>


                                            </select>
                                        </div>
                                        <span class="lq-msgvalid">Ingresar el día</span>
                                    </div>
                                    <div class="lq-parent lq-ingroup lq-mes">
                                        <div class="lq-pull lq-noborder">
                                            <select name="entry.245093043" id="imes_cony" data-valid="required" class="novalid">
                                                <option value="">Mes</option>
                                                <option value="1">Enero</option>
                                                <option value="2">Febrero</option>
                                                <option value="3">Marzo</option>
                                                <option value="4">Abril</option>
                                                <option value="5">Mayo</option>
                                                <option value="6">Junio</option>
                                                <option value="7">Julio</option>
                                                <option value="8">Agosto</option>
                                                <option value="9">Setiembre</option>
                                                <option value="10">Octubre</option>
                                                <option value="11">Noviembre</option>
                                                <option value="12">Diciembre</option>
                                            </select>
                                        </div>
                                        <span class="lq-msgvalid">Ingresar el mes</span>
                                    </div>
                                    <div class="lq-parent lq-ingroup lq-anio">
                                        <div class="lq-pull lq-noborder">
                                            <select name="entry.2050828489" id="ianio_cony" data-valid="required" class="novalid">
                                                <option value="">Año</option>
                                                <?php
                                                for($i = 1910; $i<=1998; $i++){
                                                ?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php
                                                    }
                                                ?>

                                            </select>
                                        </div>
                                        <span class="lq-msgvalid">Ingresar el año</span>
                                    </div>
                                    <div class="both"></div>
                                </div>
                                
                                <h6 class="lq-h6">Estado civil del cónyugue *</h6>
                                <div class="lq-parent">
                                    <div class="lq-pull">
                                        <select name="iestado_cony" id="iestado_cony" data-valid="required" class="novalid">
                                            <option value="">Seleccione estado civil</option>
                                            <option value="1">Soltero</option>
                                            <option value="4">Divorciado</option>
                                            <option value="5">Conviviente</option>
                                            <option value="6">Viudo</option>
                                            <option value="7">Casado</option>
                                            <option value="8">Casado con Bienes separados</option>
                                        </select>
                                        <input type="hidden" id="iestado_cony_hidden" name="entry.874706033">
                                    </div>
                                    <span class="lq-msgvalid">Ingresar correctamente</span>
                                </div>
                                
                                <h6 class="lq-h6">Datos laborales del cónyugue *</h6>
                                <div class="change_situacion">
                                    <div class="lq-parent">
                                        <div class="lq-pull">
                                            <select name="isituacion_cony" id="isituacion_cony" data-valid="required" class="novalid">
                                                <option value="">Seleccione situación laboral</option>
                                                <option value="1">Dependiente</option>
                                                <option value="3">Accionista empresa</option>
                                                <option value="4">Profesional independiente</option>
                                                <option value="5">Rentista 1 categoría</option>
                                                <option value="6">Rentista 2 categoría</option>
                                                <option value="7">Microempresario formal</option>
                                                <option value="9">Jubilado</option>
                                                <option value="10">Informal</option>
                                                <option value="11">No laboro actualmente</option>
                                            </select>
                                            <input type="hidden" id="isituacion_cony_hidden" name="entry.1220329917">
                                        </div>
                                        <span class="lq-msgvalid">Ingresar correctamente</span>
                                    </div>
                                    <div class="lq-parent fixruc">
                                        <div class="lq-input lq-number">
                                            <input type="text" name="entry.223234284" id="iruc_cony" data-holder="Nro RUC" maxlength="11">
                                        </div>
                                        <span class="lq-msgvalid">Ingresar correctamente</span>
                                        <div class="lq-input-info">Encuentra tu RUC <a href="#">aquí</a></div>
                                        
                                    </div>
                                </div>
                                
                                <div id="dataconyugue_ce" style="display:none">
                                    <h6 class="lq-h6">Tiempos en años y meses del cónyugue *</h6>
                                    <div class="lq-group-element">
                                        <div class="lq-parent lq-tiempo-a lq-ingroup">
                                            <div class="lq-input lq-number">
                                                <input type="text" name="entry.1716836074" id="itanios_cony" data-valid="required" class="novalid" maxlength="2">
                                            </div>
                                            <!--<span class="lq-msgvalid">Ingresar los <br>años</span>-->
                                            <div class="lq-tooltip">
                                                <div>
                                                    <div class="lq-tooltip-body"><div>Señala cuánto tiempo vives en el país. Debes completar los campos de años y meses.</div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="lq-tiempo-txt">Años</div>
                                        
                                        <div class="lq-parent lq-tiempo-m lq-ingroup">    
                                            <div class="lq-input lq-number lq-once">
                                                <input type="text" name="entry.176361042" id="itmeses_cony" data-valid="required" class="novalid" maxlength="2">
                                            </div>
                                            <!--<span class="lq-msgvalid">Ingresar los <br>meses</span>-->
                                            <div class="lq-tooltip lq-toolright">
                                                <div>
                                                    <div class="lq-tooltip-body"><div>En caso que el número de años y/o meses sea menor a 1, por favor, ingresa el número 0.</div></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="lq-tiempo-txt">Meses</div>
                                            
                                        <div class="both"></div>
                                    </div>               
                                    <div class="lq-parent">
                                        <div class="lq-input lq-alphanum">
                                            <input type="text" name="ipasaporte_cony" id="ipasaporte_cony" data-valid="pasaporte" data-holder="Nro Pasaporte" class="novalid" maxlength="11">
                                        </div>
                                        <span class="lq-msgvalid">Ingresar correctamente</span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="lq-group-captcha">
                    <!-- 
                        <div class="lq-captcha">
                            <div class="lq-img"><img src="img/captcha.jpg"></div>
                            <i></i>
                            <div class="lq-tooltip">
                                <div>
                                    <div class="lq-tooltip-body"><div>Ingresa los valores que ves en el cuadro</div></div>
                                </div>
                            </div>
                        </div>
                        <div class="lq-parent">
                            <div class="lq-input lq-alphanum">
                                <input type="text" name="icaptcha" id="icaptcha" data-valid="required" data-holder="Ingresar código" />
                            </div>
                            <span class="lq-msgvalid">Ingresar correctamente</span>
                        </div>
                        <div class="both"></div>
 -->
                    </div>


                    <div class="lq-item lq-acenter">
                        <div class="lq-check">
                            <input type="checkbox" id="iterminos" name="entry.1825212905" data-valid="required" autocomplete="off" value="Acepto términos y condiciones">
                        </div> 
                        <label class="lq-label" for="terminos">Acepto los <a href="#">términos y condiciones</a></label>
                        <span class="lq-msgvalid">Debes aceptar los términos y condiciones</span>
                    </div>
                    <div class="lq-div-alert">&nbsp;</div>
                    <button type="button" class="lq-button" id="lq-btn-verify">Verificar<i></i></button>
                    
                </form>
                <div class="both"></div>
            </div>     
                    
            <div class="form_message thanks_message">
                <h2 class="">
                    Gracias
                </h2>
                <p>
                    Ya tenemos tus datos y pronto nos comunicaremos contigo.
                </p>
            </div>

        </div>        
    </div>


    <!-- #footer -->
    <div class="lq-footer">
        <div class="lq-foot">
        	<div class="lq-foot1">
            	Si tienes dudas o consultas, contacta con nuestra banca <br>
                telefónica, <strong>disponible las 24hrs.</strong>
            </div>
            <div class="lq-foot2">Lima <strong>311 9000</strong></div>
            <div class="lq-foot2">Provincias <strong>0 801 00802</strong></div>
        </div>
        <div class="lq-copy">
        	<div class="lq-wrap">
            	"La empresa tiene la obligación de difundir información de conformidad con la Ley N° 28587 y el 
                Reglamento de Transparencia de Información y Disposiciones <br>
                Aplicables a la Contratación con Usuarios del Sistema Financiero, aprobado mediante Resolución SBS N° 8181-2012."<br>
                
                &copy; Copyright 2013 - Todos los derechos reservados
            </div>
        </div>
    </div>
    <!-- footer -->


    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="http://hc-visaoromembresia-html.apps-interbank.com/html-adq/js/alphanumeric.js"></script>
    <script src="http://hc-visaoromembresia-html.apps-interbank.com/html-adq/js/main.js"></script>
    <script src="http://hc-visaoromembresia-html.apps-interbank.com/html-adq/js/tarjeta-credito-1era-adq-dni.js"></script>

    <!-- ADQ form -->



</div>


</body>
</html>
